const Error_code = {
    EmptyInput: {
        mess: "Pls Input Full",
        err: "ERR_001"
    },
    InvalidPass: {
        mess: "Invalid Password!",
        err: "ERR_002"
    },
    SignIn_Success: {
        mess: "SignIn Success",
        err: null
    },
    SignIN_F_U_P: {
        mess: "Invalid Username or Password",
        err: "ERR_003"
    },
    SignUp_Success: {
        mess: "SignUp Success",
        err: null
    },
    SignUp_F_U_P: {
        mess: "Invalid Username or Password",
        err: "ERR_004"
    },
    Exists_Email: {
        mess: "Email is Exists",
        err: "ERR_005"
    },
    getProfile_Success: {
        mess:"Get Profile Success",
        err:null,
    },
    getProfile_Failure: {
        mess:"Get Profile Failure",
        err:"ERR_006",
    }
}
const SignIn_Success = (mess = "SignIn Success", err = null ,token = "") => ({
    status: "OK",
    message: mess,
    error: err,
    auth_token: token,
})
const SignIn_Failure = (mess = "SignIn Failure", err = null) => ({
    status: "ERROR",
    message: mess,
    error: err,
    auth_token: null,
})

const SignUp_Success = (mess = "SignUp Success", err = null) => ({
    status: "OK",
    message: mess,
    error: err,
})
const SignUp_Failure = (mess = "SignUp Failure", err = null) => ({
    status: "ERROR",
    message: mess,
    error: err,
})
const getProfile_Success = (mess = "Get Profile Success", err = null, data) => ({
    status: "OK",
    message: mess,
    error: err,
    data
})
const getProfile_Failure = (mess = "Get Profile Failure", err = null) => ({
    status: "ERROR",
    message: mess,
    error: err,
})
module.exports ={
    Error_code,
    SignIn_Success,
    SignIn_Failure,
    SignUp_Success,
    SignUp_Failure,
    getProfile_Success,
    getProfile_Failure
}