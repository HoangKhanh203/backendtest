const mongoose = require("mongoose");



const FurnitureSchema = new mongoose.Schema({
    id: {
        type: Number
    },
    title: {
        type: String
    },
    image: {
        type: String
    },
    description: {
        type: String
    },
    categoriesID: {
        type: Number
    }
})

module.exports = mongoose.model("FURNITURE",FurnitureSchema,"FURNITURE" );