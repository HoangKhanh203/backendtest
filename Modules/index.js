const Furniture = require("./furniture.models");
const Categories = require("./categories.models");
const Account = require("./accounts.models");


module.exports = {
    Furniture,
    Categories,
    Account
}