const mongoose =require("mongoose");

const CategoriesSchema = new mongoose.Schema({
    title: {
        type: String
    },
    image: {
        type: String
    },
    description: {
        type: String
    }
})
module.exports = mongoose.model("CATEGORIES",CategoriesSchema,"CATEGORIES");
