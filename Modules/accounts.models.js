const mongoose = require("mongoose");

const SignUpSchema = new mongoose.Schema({
    email: {
        type: String
    },
    username: {
        type: String
    },
    password: {
        type: String
    },
    name: {
        type: String
    },
    token: {
        type: String
    }
})
module.exports = mongoose.model("ACCOUNT",SignUpSchema,"ACCOUNT");