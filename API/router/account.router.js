const express = require("express");

const router = express();
const controller = require("../controller");
const { Account } = controller;

router.get("/signin",Account.signIn);
router.get("/signup",Account.signUp);
router.get("/getprofile",Account.getProfile);


module.exports  = router