const Furniture = require("./furniture.router");
const Account = require("./account.router");
module.exports = {
    Furniture,
    Account
}