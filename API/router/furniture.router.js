const express = require("express");
const routerFurniture = express.Router();

const controllers = require("../controller");
const { getProduct } = controllers;

routerFurniture.get("/allfurniture", getProduct.getAllFurniture);
routerFurniture.get("/allcategories", getProduct.getAllCategories);
routerFurniture.get("/furnitureincategoriesbyid?:id", getProduct.getFurnitureInCategoriesById);


module.exports = routerFurniture;