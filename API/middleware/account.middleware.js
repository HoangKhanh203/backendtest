const { get } = require("lodash");
const response = require("../../response");
const { Account } = response;
const { Error_code } = Account;
const { EmptyInput, InvalidPass } = Error_code;
const signIn = (req,res ,next) => {
    const query = get(req,"query", {})
    const username = get(query,"username", "");
    const password = get(query,"password", 0);

    if(!username || !password )
         res.json(Account.SignIn_Failure(EmptyInput.mess,EmptyInput.err))
    if(typeof parseInt(password) !== "number")
         res.json(Account.SignIn_Failure(InvalidPass.mess,InvalidPass.err))

    next();
}
const signUp = (req,res ,next) => {
    const query = get(req,"query", {})
    const username = get(query,"username", "");
    const password = get(query,"password", 0);
    const email = get(query,"email", "");

    console.log("email:" ,email);
    if(!username || !password || !email )
         res.json(Account.SignUp_Failure(EmptyInput.mess,EmptyInput.err))
    if(typeof parseInt(password) !== "number")
         res.json(Account.SignUp_Failure(InvalidPass.mess,InvalidPass.err))

    next();
}



module.exports ={
    signIn,
    signUp
}