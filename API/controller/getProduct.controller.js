const { Module } = require("module");
const Models = require("../../Modules/index");
const { get } = require("lodash");

/*--------------- Function Support---------------- */

const getDataFurniture = async () => await Models.Furniture.find();

const getDataCategories = async () => await Models.Categories.find();


/*--------------- Main---------------- */
const getAllFurniture = async (req,res) => {
    const dataFurniture = await getDataFurniture();
    res.json({
        data: {
            furnitures: dataFurniture
        }
    });
};
const getAllCategories = async (req,res) => {
    const dataCategories = await getDataCategories();
    res.json({
        data: {
            categories: dataCategories
        }
    });
};

const getFurnitureInCategoriesById = async (req,res) => {
    const params = get(req,"params",{});
    const query = get(req,"query",{});

    const ID =parseInt( get(query,"id",-1));
    if(typeof ID !== "number" || ID === -1 ){
        return res.json("Id Not Exists");
    }
    const dataFurniture = await getDataFurniture();
    const arrItemInFurniture = dataFurniture.filter(item => get(item,"categoriesID", -1) === ID);
    res.json({
        data: {
            furnitures: arrItemInFurniture
        }
    });
}


module.exports = {
    getAllFurniture,
    getAllCategories,
    getFurnitureInCategoriesById
}
