const randomString = require("randomstring");
const { get } = require("lodash");

const Models = require("../../Modules");
const response = require("../../response");
const e = require("express");

const { Account } = response;
const { Error_code } = Account;
const {
  SignIn_Success,
  SignUp_Success,
  Exists_Email,
  SignIN_F_U_P,
  getProfile_Failure,
  getProfile_Success,
  SignUp_F_U_P,
  EmptyInput,
  InvalidPass,
} = Error_code;

const getDataUserByEmail = async (_email) =>
  await Models.Account.findOne({ email: _email });
const getDataUserByUserName = async (_username, _password) =>
  await Models.Account.findOne({ username: _username, password: _password });

const getDataUserByToken = async (_token) =>
  await Models.Account.find();

const signIn = async (req, res) => {
  const query = get(req, "body", {});
  const username = get(query, "username", "");
  const password = get(query, "password", 0);
  if (!username || !password)
    res.json(Account.SignIn_Failure(EmptyInput.mess, EmptyInput.err));
  if (typeof parseInt(password) !== "number")
    res.json(Account.SignIn_Failure(InvalidPass.mess, InvalidPass.err));

  const createToken = randomString.generate(100);
  const dataUser = await getDataUserByUserName(username, password);

  if (!dataUser)
    res.json(
      Account.SignIn_Failure(SignIN_F_U_P.mess, SignIN_F_U_P.err, createToken)
    );
  else {
    Models.Account.updateOne(
      { email: dataUser.email },
      { $set: { token: createToken } }
    ).then(() => {});
    res.json(Account.SignIn_Success(SignIn_Success.mess, SignIn_Success.err, createToken));
  }
};

const signUp = async (req, res) => {
  const query = get(req, "body", {});
  const email = get(query, "email", "");
  const username = get(query, "username", "");
  const password = get(query, "password", 0);

  if (!username || !password || !email)
    res.json(Account.SignUp_Failure(EmptyInput.mess, EmptyInput.err));
  if (typeof parseInt(password) !== "number")
    res.json(Account.SignUp_Failure(InvalidPass.mess, InvalidPass.err));
  else {
    const dataUser = await getDataUserByEmail(email);
    const createToken = randomString.generate(20);
    if (!dataUser) {
      const createAccount = new Models.Account({
        email: email,
        username: username,
        password: password,
        token: createToken,
      });
      createAccount.save((err) => {
        if (err) console.log("err: ", err);
        console.log("don");
      });
      res.json(Account.SignUp_Success(SignUp_Success.mess, SignUp_Success.err));
    } else {
      res.json(Account.SignUp_Failure(Exists_Email.mess, Exists_Email.err));
    }
  }
};

const getProfile = async (req, res) => {

    const query = get(req, "body", {});

  const token = get(query, "token", "");

  if(!token)
    return res.json(Account.getProfile_Failure(getProfile_Failure.mess,getProfile_Failure.err))
  
    const dataUser = await getDataUserByToken(token);
  if (!dataUser)
    res.json(
      Account.getProfile_Failure(
        getProfile_Failure.mess,
        getProfile_Failure.err
      )
    );
  else {
    res.json(
      Account.getProfile_Success(
        getProfile_Success.mess,
        getProfile_Success.err,
        dataUser
      )
    );
  }
};

module.exports = {
  signIn,
  signUp,
  getProfile,
};
