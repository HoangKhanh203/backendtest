const express = require("express");
const cors = require('cors');
const mongoose = require("mongoose");
const bodyParser = require("body-parser");

mongoose.connect(
  "mongodb+srv://hoangkhanh:123456789Kh@cluster0.dzqb6.mongodb.net/DEMOSTURE?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);
const app = express();
const port = 5000;
app.use(cors({
  origin: '*',
  methods: [
    'GET',
    'POST',
  ],

  allowedHeaders: [
    'Content-Type',
  ], 
}))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const ApiRouter = require("./API/router");
const ApiMiddleware = require("./API/middleware");


app.use("/api/furniture", ApiRouter.Furniture);
app.use("/api/account",  ApiRouter.Account);


app.listen((process.env.PORT || 5000), () => {
    console.log(`start at localhost:${port}`)
})

// const Models = require("./Modules");
// const fs = require('fs');
// let dataJson = fs.readFileSync('data.json');
// const { data_cate } = JSON.parse(dataJson);
// const data = data_cate;
//   for(let index in data){
//     const insert = Models.Categories({
//   id: index,
//         title: data[index].title,
//         image: data[index].urlImage,
//         description: data[index].description,
//     });
//     insert.save((err) => {
//       if (err) console.log(err);
//       console.log('done.');
//     });
// }



// const Models = require("./Modules");
// const fs = require('fs');
// let dataJson = fs.readFileSync('data.json');
// const { data_fur } = JSON.parse(dataJson);
// const data = data_fur;
//   for(let index in data){
//     const insert = Models.Furniture({
//         id: index,
//         title: data[index].title,
//         image: data[index].urlImage,
//         description: data[index].description,
//         categoriesID: Math.floor(Math.random() * 4)
//     });
//     insert.save((err) => {
//       if (err) console.log(err);
//       console.log('done.');
//     });
// }